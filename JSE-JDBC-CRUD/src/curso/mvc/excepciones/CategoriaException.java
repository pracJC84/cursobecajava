package curso.mvc.excepciones;

/*
 * Excepcion personalizada NO comprobada. Cuando se lanza (throw)
 * 
 * obligatoriamente hay que propagarla (throws)
 */
public class CategoriaException extends Exception {

	private static final long serialVersionUID = 666L;

	public CategoriaException(String mensaje, Throwable excepcion) {
		super(mensaje, excepcion);
	}

	public CategoriaException(String mensaje) {
		super(mensaje);
	}

}

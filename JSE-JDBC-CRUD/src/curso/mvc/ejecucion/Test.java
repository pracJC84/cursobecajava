package curso.mvc.ejecucion;

import curso.mvc.excepciones.CategoriaException;
import curso.mvc.modelo.CategoriaDAO;
import curso.mvc.modelo.CategoriaDAOImpl;
import curso.mvc.modelo.CategoriaDTO;
import curso.mvc.servicios.CategoriaService;
import curso.mvc.servicios.CategoriaServiceImpl;

public class Test {

	public static void main(String[] args) {

		try {
			CategoriaDAO dao = new CategoriaDAOImpl();
			
			CategoriaService service = new 
					CategoriaServiceImpl(dao);
			
			service.add(new CategoriaDTO(
					"IT_JAVA", "Programador Java I+D+I", 
						18000, 25000));
			
			CategoriaDTO dto = service.findById("IT_JAVA");
			
			dto.setCategoria("Programador Junior Java");
			dto.setSalarioMaximo(30000);
			
			service.edit(dto);
			
			System.out.println(service.findAll());
			
			service.remove("IT_JAVA");
			
			System.out.println(service.findById("PU_MAN"));
			
			System.out.println(service.findById("PU_MAN2222"));
			
		} catch (CategoriaException e) {
			System.out.println(e.getMessage() + "\n");
			e.printStackTrace();
		}
	}
}

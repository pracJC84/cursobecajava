package curso.mvc.modelo;

import java.io.Serializable;

/*
 * Data Transfer Object (DTO) que encapsula la tabla JOBS
 * 
 * Clase serializables
 * 
 * Patr�n (Estructura de Glasgow)
 * 
 * Campo/Propiedad privada por cada columna de la tabla
 * 
 * M�todos Get/Set p�blicos
 * 
 * Constructores
 */
public class CategoriaDTO implements Serializable {

	/**
	 * Version fija para la clase
	 */
	private static final long serialVersionUID = 666L;

	/**
	 * Campos para encapsular las columnas de la tabla
	 */
	private String codigo;
	private String categoria;
	private double salarioMinimo;
	private double salarioMaximo;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public double getSalarioMinimo() {
		return salarioMinimo;
	}

	public void setSalarioMinimo(double salarioMinimo) {
		this.salarioMinimo = salarioMinimo;
	}

	public double getSalarioMaximo() {
		return salarioMaximo;
	}

	public void setSalarioMaximo(double salarioMaximo) {
		this.salarioMaximo = salarioMaximo;
	}

	public CategoriaDTO(String codigo, String categoria, double salarioMinimo, double salarioMaximo) {
		this.codigo = codigo;
		this.categoria = categoria;
		this.salarioMinimo = salarioMinimo;
		this.salarioMaximo = salarioMaximo;
	}

	public CategoriaDTO(String codigo, String categoria) {
		this(codigo, categoria, 0, 0);
	}

	public CategoriaDTO() {
		
	}

	@Override
	public String toString() {
		return "\nCategoriaDTO [codigo=" + codigo + ", categoria=" + categoria + ", salarioMinimo=" + salarioMinimo
				+ ", salarioMaximo=" + salarioMaximo + "]";
	}
	
	
}

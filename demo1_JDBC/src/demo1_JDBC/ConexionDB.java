package demo1_JDBC;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {
    private static Connection cn = null;
    private static Driver driver = new oracle.jdbc.OracleDriver();
    private static String URLOracle = "jdbc:oracle:thin:@localhost:1521:XE";
    private static String usuario = "pedro";
    private static String contrasena = "admin";
   
    public static Connection getConexion() throws SQLException {
        DriverManager.registerDriver(driver);
        cn = DriverManager.getConnection(URLOracle, usuario, contrasena);
        return cn;
    }
}
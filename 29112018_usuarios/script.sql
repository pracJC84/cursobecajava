create table passw_(
    pk_pid number PRIMARY KEY,
    pass varchar(32) not null
);

create table rol(
    pk_rid number primary key,
    rdesc varchar(32) not null
);

create table user_(
    pk_uid number primary key,
    uname varchar(32),
    fk_pid number not null,
    fk_rid number not null,
    FOREIGN key (fk_pid) REFERENCES passw_(pk_pid),
    FOREIGN key (fk_rid) REFERENCES rol(pk_rid)
);

create table inventory(
    pk_iud number PRIMARY KEY,
    iname varchar(32) not null
);

create table permiss(
    pk_pid number primary key,
    fk_rid number not null,
    fk_iid number not null,
    FOREIGN key (fk_rid) REFERENCES rol(pk_rid),
    FOREIGN key (fk_iid) REFERENCES inventory(pk_iid)
);

insert into passw_ values (1, '1111');
insert into passw_ values (2, '1111');
insert into passw_ values (3, '1111');

insert into rol values (1, 'god mode');
insert into rol values (2, 'decent one');
insert into rol values (3, 'worthless');

insert into user_ values (1, 'pedro', 1, 1);
insert into user_ values (2, 'foo()', 2, 2);
insert into user_ values (3, 'luis', 3, 3);

insert into inventory values(1, 'genetic');
insert into inventory values (2, 'tecnology');
insert into inventory values (3, 'html');

insert into permiss values (1, 1, 1);
insert into permiss values (2, 1, 2);
insert into permiss values (3, 2, 2);
insert into permiss values (4, 3, 3);

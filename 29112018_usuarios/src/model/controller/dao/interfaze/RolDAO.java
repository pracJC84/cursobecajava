package model.controller.dao.interfaze;

import java.util.List;

import model.entidad.Rol;

public interface RolDAO{

	void create(Rol rol) throws Exception;
	
	void edit(Rol rol);
	
	void delete(int rolId);
	
	List<Rol> findAll();

	Rol findById(int i);
	
}

package model.controller.dao.interfaze;

import java.util.List;

import model.entidad.Permiss;

public interface PermissDAO{

	void create(Permiss permiss);
	
	void edit(Permiss permiss);
	
	void delete(int permissId);
	
	List<Permiss> findAll();

	Permiss findById(int i);
	
}

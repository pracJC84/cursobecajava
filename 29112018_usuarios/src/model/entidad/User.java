package model.entidad;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="COGEN")
@NamedQueries(
		@NamedQuery(name="User.findAll", query="select u from User u")
)
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5883962324558861943L;

	@Id
	@Column(name="PK_UID")
	private int id;
	
	@Column(name="UNAME")
	private String name;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="FK_PID")
	private Pass pass;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="FK_RID")
	private Rol rol;

	public int getId() {
		return id;
	}

	public void setId(int uid) {
		this.id = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String uname) {
		this.name = uname;
	}

	public Pass getPass() {
		return pass;
	}

	public void setPass(Pass pass) {
		this.pass = pass;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	@Override
	public String toString() {
		return "User [uid=" + id + ", uname=" + name + "\npass=" + pass.toString() + "\n rol=" + rol.toString() + "]";
	}

	public User() {
		super();
	}

	public User(int i, String string) {
		this.id = i;
		this.name = string;
	}
	
	
}

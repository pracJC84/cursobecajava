package model.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="ROL")

public class Rol implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4991030605240231538L;
	
	@Id
	@Column(name = "PK_RID")
	private int id;
	
	@Column(name = "RDESC")
	private String desc;

	public Rol() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Rol(int id, String desc) {
		super();
		this.id = id;
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "Rol [id=" + id + ", desc=" + desc + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

package exe;

import model.controller.dao.implement.PassDAOImpl;
import model.controller.dao.interfaze.PassDAO;
import model.entidad.Pass;

public class TestPass {

	public static void main(String[] args) {
try {
			
			PassDAO dao = new PassDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			
			dao.create(new Pass(10, "1111"));
			
			//dao.delete(7);
			Pass u = dao.findById(10);
			
			System.out.println(u.toString());
			
			dao.delete(u.getId());
			
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}

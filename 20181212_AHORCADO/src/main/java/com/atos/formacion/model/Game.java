package com.atos.formacion.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Game {
	public static final int MAX_ERRORS = 5;
	public static final String INITIAL_WORD = "papaya";
	private String word;
	private List<String> letters;
	private int errors;
	
	private boolean rightLetter(char l) {
		if(letters.contains(l+"") || !word.contains(l+"")) {
			return false;
		}
		
		return true;
	}
	/**
	 * 
	 * @param letter
	 * @return returns true if the word contains the letter and u didn't type the letter before
	 */
	public boolean setLetter(char l) {
		if(!rightLetter(l)) {
			if(!word.contains(l+""))
				letters.add(l+"");
			return false;
		}
		letters.add(l+"");
		return true;
	}
	
	public Game() {
		this.word = INITIAL_WORD;
		letters = new ArrayList<String>();
		errors = 0;
	}
	
	public Game(String word) {
		this.word = word;
		letters = new ArrayList<String>();
		errors = 0;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
		this.restart();
	}
	
	public List<String> getLetters() {
		return letters;
	}
	public void addError() {
		errors++;		
	}
	public int getErrors() {
		return errors;
	}
	public void setErrors(int errors) {
		this.errors = errors;
	}
	public boolean isWinned() {
		try {
			for(int i = 0; i < getWord().length(); i++) {
				
					if(!getLetters().contains(Character.toString(getWord().charAt(i)))){
						return false;
				
			}
		}
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	public boolean isLoosed() {
		return getErrors() >= MAX_ERRORS;
		
	}
	public void playLetter(char letter) {
		if(!setLetter(letter)) {
			setErrors(getErrors()+1);
		}
		
	}
	public void restart() {
		letters=new ArrayList<String> ();
		setErrors(0);
	}
	
}

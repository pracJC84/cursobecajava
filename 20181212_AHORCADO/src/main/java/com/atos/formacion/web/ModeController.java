package com.atos.formacion.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.atos.formacion.model.Game;

@Controller
public class ModeController {
	@Autowired
	Game game;
	
	@RequestMapping(value = { "/index", "/" }, method = RequestMethod.GET)
	public ModelAndView index1() {
		game.restart();
		return new ModelAndView("index");

	}
	
	@RequestMapping(value = { "/play" }, method = RequestMethod.GET)
	public ModelAndView play(@ModelAttribute("game") Game game) {
		return new ModelAndView("play", "game", game);
	}
	
	@RequestMapping(value = { "/change" }, method = RequestMethod.GET)
	public ModelAndView change() {

		return new ModelAndView("changeForm");

	}
	
}

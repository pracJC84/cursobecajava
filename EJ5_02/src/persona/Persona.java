	package persona;

import java.util.Scanner;
import utilities.Validable;
public class Persona extends Validable{
	public static final int MAYOR_EDAD = 18;
	public static enum IMC  {
		inferior("inferior"), normal("normal"), elevado("elevado");
		private final String string;
		private IMC(String s) {
	        string = s;
	    }
		public String toString() {
			return this.string;
		}
	}
	private String nombre = "";
	private int edad = 0;
	private String dNI;
	private char sexo = 'H';
	private float peso = 0;
	private float altura = 0;
	private Scanner sc = new Scanner(System.in);
	
	public IMC calcularIMC(Fitness fit) {
		double imc = fit.calcularIMC(this);
		if(imc < 20) 
			return IMC.inferior;
		
		if(imc >= 20 && imc < 25)
			return IMC.normal;
		
		return IMC.elevado;
	}

	public Persona(String nombre, int edad, char sexo, float peso, float altura) {
		this.dNI = this.requestDNI();
		this.nombre = nombre;
		this.edad = edad;
		this.dNI = this.requestDNI();
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}


	private String requestDNI() {
		String dNIInput;
		System.out.println("Introduce tu DNI, porfavor: ");
		dNIInput = this.sc.nextLine();
		
		while(!validarDNI(dNIInput)) {
			System.out.println("existe un error con el dni, porfavor, introducelo de nuevo");
			dNIInput = this.sc.nextLine();
		}
		System.out.println("DNI a�adido correctamente y es " + dNIInput);
		return dNIInput;
	}
	
	public boolean esMayorDeEdad() {
		return this.getEdad() > MAYOR_EDAD;
	}

	public Persona(String nombre, int edad, char sexo) {
		this.dNI = this.requestDNI();
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
	}


	public Persona() {
		this.dNI = this.requestDNI();
		
	}


	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", DNI=" + dNI + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		if(this.validarEdad(edad)) {
			this.edad = edad;
		}
	}


	public String getDNI() {
		return dNI;
	}


	public void setDNI(String dNI) {
		this.dNI = dNI;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		if(this.validarSexo(sexo))
			this.sexo = sexo;
	}


	public float getPeso() {
		return peso;
	}


	public void setPeso(float peso) {
		if(this.validarPeso(peso))
			this.peso = peso;
	}


	public float getAltura() {
		return altura;
	}


	public void setAltura(float altura) {
		if(this.validarAltura(altura))
			this.altura = altura;
	}


	public static void main(String[] args) {
		Persona p = new Persona();
		Scanner sc = new Scanner(System.in);
		Fitness fit = new Fitness();
		
		System.out.println("introduce el nombre");
		p.setNombre(sc.nextLine());
		System.out.println("introduce la edad");
		p.setEdad(sc.nextInt());
		System.out.println("introduce sexo");
		sc.nextLine();
		p.setSexo(sc.nextLine().charAt(0));
		System.out.println("introduce peso");
		try {
			p.setPeso(Float.parseFloat(sc.nextLine()));
		}catch(Exception e) {
			p.setPeso(0);		
		}
		System.out.println("introduce altura");
		try {
			p.setAltura(Float.parseFloat(sc.nextLine()));
		}catch (Exception e) {
			
		}
		System.out.println(p.toString());
		Persona p1 = new Persona(p.getNombre(), p.getEdad(),  p.getSexo(), p.getPeso(), p.getAltura());
		Persona p2 = new Persona(p.getNombre(), p.getEdad(), p.getSexo());
		Persona[] ps = {p1, p2, p}; 
		
		for(Persona x : ps) {
			IMC i = x.calcularIMC(fit);
			System.out.println("la persona con dni: " +  x.getDNI() + " tiene un peso " + i.toString());
			System.out.println("y es " + x.esMayorDeEdad());
		}
		
		sc.close();
		
	}

}

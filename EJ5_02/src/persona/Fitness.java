package persona;

public class Fitness {

	public float calcularIMC(Persona p){
		float peso = p.getPeso();
		float altura = p.getAltura();
		return (float) (peso / Math.pow(altura, 2));
	}
}

package utilities;

import java.util.regex.Pattern;

public class Validador {
	
	public boolean sexo(char s) {
		if(s != 'H' && s != 'M') {
			return false;
		}
		return true;
	}
	private boolean validarDNI(String dni) {
		String dniRegexp = "\\d{8}[A-HJ-NP-TV-Z]";
		
		if(Pattern.matches(dniRegexp, dni))
			return true;
		
		return false;
	}
	public static void main(String[] args) {
		Validador v = new Validador();
		System.out.println(v.sexo('M'));
	}
}

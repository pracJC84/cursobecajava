package atos.mvc.controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DispatcherServlet
 */
@WebServlet("/Autentification")
public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		System.out.println("Servlet inicializado");
	}

	public void destroy() {
		System.out.println("Servlet finalizado");
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String vista;
		String name = request.getParameter("name");
		
		if(name.equals("admin")) {
			vista = "/LoginRightAdmin.jsp";
		}else if(name.equals("root")){
			vista = "/LoginRightRoot.jsp";
		}else {
			vista = "/LoginRight.jsp";
		}
		
		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(vista);

				// Reenviar peticion y respuesta al JSP (vista)
				dispatcher.forward(request, response);
		
	}

}

package exe;

import java.util.List;

import model.controller.dao.implement.BookDAOImpl;
import model.controller.dao.interfaze.BookDAO;
import model.entity.Book;

public class TestBook {
	public static void main(String[] args) {
		try {
			// Crear DAO
			BookDAO dao = new BookDAOImpl();
			
			System.out.println("Unidad de Persitencia cargada");
			
			// Recuperar entidad por codigo
			Book book = dao.findById(1);
			
			// Mostrar datos en pantalla
			mostrar(book);
			
			// Modificar datos locales
			book.setTitle("Donde cantan los arboles");
			
			// Modificar en DDBB
			dao.edit(book);
			
			Book newBook = new Book(43, "el ejercito negro", 12, "2010-12-12", 1);
			
			// Crear book
			dao.create(newBook);
			
			// Recorrer la lista de bookes
			for(Book cov : dao.findAll()) {
				mostrar(cov);
			}
			
			// Eliminar book creada
			dao.delete(newBook.getBid());
			
			for(Book cov : dao.findByAuthor(1)) {
				mostrar(cov);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void mostrar(Book book) {
		System.out.println(book.toString());		
	}
}

package model.controller.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.controller.dao.interfaze.BookDAO;
import model.entity.Book;

public class BookDAOImpl implements BookDAO{

	private EntityManagerFactory emf;

	public BookDAOImpl() {
		// Cargar unidad de persistencia
		emf = Persistence.createEntityManagerFactory("PRACTICE");
	}

	/*
	 * Gestor de Entidades. EntityManager. Se crea SIEMPRE desde
	 * EntityManagerFactory
	 * 
	 * M�todo local (privado) para devolver un EntityManager
	 */
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public void create(Book book) /* throws Exception */ {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Crear instancia entidad (hacer INSERT en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.persist(book);

			// Confirmar transaccion
			em.getTransaction().commit();

		} /*
			 * catch (Exception e) { System.out.println("Error metodo create");
			 * 
			 * throw e; }
			 */ finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void edit(Book book) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Modificar instancia entidad (hacer UPDATE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.merge(book);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void delete(int bookId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Recuperar instancia de entidad desde EntityManager
			// para que sea ATTACH
			Book book = em.getReference(Book.class, bookId);
			
			// Eliminar instancia entidad (hacer DELETE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.remove(book);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	
	@Override
	public List<Book> findAll() {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Crear objeto (Query) para ejecutar consulta
			// JPQL estatica (creada con @NamedQuery en el
			// fichero de entidad)
			Query query = em.createNamedQuery("Book.findAll");

			// Devolver lista entidades 
			return query.getResultList();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public Book findById(int bookId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Devolver instancia entidad recuperada por su campo
			// clave
			return em.find(Book.class, bookId);

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public List<Book> findByAuthor(int authorId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();
			Query query = em.createNamedQuery("Book.findByAuthor").setParameter("author", authorId);
			
			return query.getResultList();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

}

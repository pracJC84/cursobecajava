package model.controller.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.controller.dao.interfaze.AuthorDAO;
import model.entity.Author;
import model.entity.Book;

public class AuthorDAOImpl implements AuthorDAO{

	private EntityManagerFactory emf;

	public AuthorDAOImpl() {
		// Cargar unidad de persistencia
		emf = Persistence.createEntityManagerFactory("PRACTICE");
	}

	/*
	 * Gestor de Entidades. EntityManager. Se crea SIEMPRE desde
	 * EntityManagerFactory
	 * 
	 * M�todo local (privado) para devolver un EntityManager
	 */
	private EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	@Override
	public void create(Author author) /* throws Exception */ {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Crear instancia entidad (hacer INSERT en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.persist(author);

			// Confirmar transaccion
			em.getTransaction().commit();

		} /*
			 * catch (Exception e) { System.out.println("Error metodo create");
			 * 
			 * throw e; }
			 */ finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void edit(Author author) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Modificar instancia entidad (hacer UPDATE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.merge(author);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public void delete(int authorId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Abrir transaccion
			em.getTransaction().begin();

			// Recuperar instancia de entidad desde EntityManager
			// para que sea ATTACH
			Author author = em.getReference(Author.class, authorId);
			
			// Eliminar instancia entidad (hacer DELETE en BBDD
			// asociada al EntityManagerFactory con el que hemos
			// creado nuestro EntityManager)
			em.remove(author);

			// Confirmar transaccion
			em.getTransaction().commit();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}


	
	@Override
	public List<Author> findAll() {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Crear objeto (Query) para ejecutar consulta
			// JPQL estatica (creada con @NamedQuery en el
			// fichero de entidad)
			Query query = em.createNamedQuery("Author.findAll");

			// Devolver lista entidades 
			return query.getResultList();

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}

	@Override
	public Author findById(int authorId) {
		EntityManager em = null;
		try {
			// Crear EntityManager
			em = getEntityManager();

			// Devolver instancia entidad recuperada por su campo
			// clave
			return em.find(Author.class, authorId);

		} finally {
			// Comprobar si se ha creado el EntityManeger
			if (em != null) {
				// Cerrar y liberar conexion y recursos
				em.close();
			}
		}
	}
}

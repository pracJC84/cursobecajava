package model.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "COVER")
@NamedQuery(name = "Cover.findAll", query = "select c from Cover c")
public class Cover implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3239135699372655970L;
	
	
	@Id
	@Column(name = "CID")
	private int cid;
	
	@Column(name = "CFILE")
	private String cfile;
	
	@Column(name = "CNAME")
	private String cname;

	public Cover() {
	}

	public Cover(int cid, String cfile, String cname) {
		super();
		this.cid = cid;
		this.cfile = cfile;
		this.cname = cname;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getCfile() {
		return cfile;
	}

	public void setCfile(String cfile) {
		this.cfile = cfile;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	@Override
	public String toString() {
		return "Cover [cid=" + cid + ", cfile=" + cfile + ", cname=" + cname + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		return cid == ((Cover) obj).cid;
	}
	
	
}

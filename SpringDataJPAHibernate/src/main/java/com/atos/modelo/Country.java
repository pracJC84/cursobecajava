package com.atos.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="COUNTRIES", schema="HR")

public class Country implements Serializable {

	private static final long serialVersionUID = 666L;

	/*
	 * Si se va a utilizar JdbcTemplate y BeanPropertyRowMapper poner campos con el
	 * mismo nombre que las columas con escritura CaMeL REGION_ID, REGIONID =>
	 * regionId
	 */
	
	@Id
	@Column(name="COUNTRY_ID")
	private String countryId;
	
	@Column(name="COUNTRY_NAME")
	private String countryName;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name="REGION_ID")
	private Region region;
	
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	@Override
	public String toString() {
		return "Country [countryId=" + countryId + ", countryName=" + countryName + ", region=" + region.getRegionName() + "]";
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public Region getRegionId() {
		return region;
	}
	public void setRegionId(Region regionId) {
		this.region = regionId;
	}
	
	public Country(String countryId, String countryName) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
	}
	public Country() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


}

package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Country;

public interface CountryDAO {

	List<Country> findAll();
	
	List<Country> findByRegion(int regionId);

}

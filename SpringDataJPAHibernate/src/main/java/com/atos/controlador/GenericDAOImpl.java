package com.atos.controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;


public class GenericDAOImpl<T, PK> implements GenericDAO<T, PK> {

	@PersistenceContext
	private EntityManager em;
	
	private Class<T> classType;
	
	public GenericDAOImpl(Class<T> classType) {
		this.classType = classType;
	}
	
	@Override
	public void create(T objectT) {
		em.persist(objectT);
	}

	@Override
	public void edit(T objectT) {
		em.merge(objectT);
		
	}

	@Override
	public void delete(PK pkId) {
		
		em.remove(em.getReference(classType, pkId));
		
	}

	@Override
	public T findById(PK pkId) {
		return em.find(classType, pkId);

	}

	
	
	@Override
	public List<T> findAll(String pkName) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		
		CriteriaQuery<T> query = 
				builder.createQuery(classType);
		
		// Clausula FROM consulta para recuperar datos 
		Root<T> root = query.from(classType);
		
		// Hacer consulta de todos los campos ordenando por
		// campo regionId
		query.select(root).orderBy(builder.asc(root.get(pkName)));
		
		return em.createQuery(query).getResultList();
	}

	

}

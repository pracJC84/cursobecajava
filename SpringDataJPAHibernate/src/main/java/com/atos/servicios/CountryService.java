package com.atos.servicios;

import java.util.List;

import com.atos.modelo.Country;

/*
 * Patron de Fachada (Facade) para abstraccion del DAO
 * mediante una capa intermedia
 */
public interface CountryService {

	void add(Country country);

	void update(Country country);

	void destroy(String countryId);

	Country getById(String countryId);

	List<Country> getAll();

	List<Country> findByRegion(int regId);

	

}

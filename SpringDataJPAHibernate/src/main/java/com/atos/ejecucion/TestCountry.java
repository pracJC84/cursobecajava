package com.atos.ejecucion;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.modelo.Country;
import com.atos.modelo.Region;
import com.atos.servicios.CountryService;
import com.atos.servicios.RegionService;

public class TestCountry {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

		CountryService servicio = (CountryService) ctx.getBean("countryServiceImpl");
		RegionService servicio2 = (RegionService) ctx.getBean("regionServiceImpl");
		
		List<Country> regions = servicio.getAll();
		System.out.println("\nNumero regiones inicio: " + regions.size());
		System.out.println(regions);

		Country newCountry = new Country();
		
		
//		Region newRegion = new Region();
//		newRegion.setRegionId(22);
//		newRegion.setRegionName("Talavera");

		newCountry.setRegionId(servicio2.getById(1));
		newCountry.setCountryId("LL");
		newCountry.setCountryName("LLanuras");

		servicio.add(newCountry);
		System.out.println("\nCountry insertada");

		Country region2 = servicio.getById("AU");
		region2.setCountryName(region2.getCountryName() + "!!!!");
		servicio.update(region2);
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones actuales: " + regions.size());
		System.out.println(regions);

		servicio.destroy("LL");
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones finales: " + regions.size());
		System.out.println(regions);
		
		System.out.println(servicio.findByRegion(1));
		
		ctx.close();
	}
	
}

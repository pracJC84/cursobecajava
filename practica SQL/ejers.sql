---1------

select * from emp;

---2------

select * from dept;

---3------

select * from emp where job = 'CLERK';

---4------

select * from emp where job = 'CLERK' order by ename;

---5------

select empno, sal, ename from emp;

---6------

select dname from dept;

---7------

select dname from dept order by dname;

---6------

select dname from dept;

---7------

select dname from dept order by dname;

---8------

select dname from dept order by location;

---9------

select dname from dept order by location desc;

---10------

select ename, job from emp order by sal;

---11------

select ename, job from emp order by job, sal;

---12------

select ename, job from emp order by job desc, sal;

---13------

select sal, comm from emp where deptno = 30;

---14------

select sal, comm from emp where deptno = 30 order by comm;

---15------

select DISTINCT comm from emp;

---16------

select DISTINCT ename, comm from emp;

---17------

select DISTINCT ename, sal from emp;

---18------

select DISTINCT ename, comm from emp;

---19------

select DISTINCT ename, sal + 1000 from emp where deptno = 30;

---20------

select DISTINCT ename, sal + 1000 from emp where deptno = 30;

---21------

select * from emp where comm > sal/2;

---22------

select * from emp where comm is null or comm <= sal * 0.25;

---23------


---24------

select empno, sal, comm from emp where empno > 7500;

---25------

select * from emp where ename > 'J%';

---26------

select ename, sal + (case
                        when comm is null then 0
                        when comm is not null then comm
                        end) as total from emp;

---27------

select * from emp where ename > 'J%' and comm is null;

---28------

select ename from emp where sal > 1000 and mgr = 7698;


---29------

select ename from emp where not(sal > 1000 and mgr = 7698);

---30------

select ename, ((case
                    when comm is null then 0
                    when comm is not null then comm
                    end) / sal) * 100 as porcentaje from emp order by ename;

---31------

select * from emp where ename not like '%LA%';

---32------

select * from emp where mgr is null;

---33------

select * from dept where dname not like 'VENTAS' and dname not like 'RESEARCH' order by location;

---34------

select ename, deptno from emp where deptno != 10 and job = 'CLERK' and sal > 800 order by hiredate;

---35------

select ename, sal/(case 
                when comm = 0 then null
                when comm != 0 then comm end) as cociente from emp where comm is not null;
                
---36------

select * from emp where ename like '_____';

---37------

select * from emp where ename like '_____%';

---38------

select * from emp where ename like 'A_' and sal > 1000 or comm is not null and deptno = 30;

---39------

select ename, sal, (case
                        when comm is null then sal
                        when comm is not null then sal + comm end) as salary from emp where ename like '_____%';

---40------

select ename, sal, comm from emp where sal > comm / 2 and sal < comm;

---41------

select ename, sal, comm from emp where ename like '_____';

---42------

select ename, job from emp where job like '%MAN' and ename like 'A%';

---43------
--para acabar--
select ename, job from emp 
where job like '%MAN' and ename like 'A%';

---44------

select * from emp where ename not like '______%';

---45------

select ename, (case
                when comm is null then sal
                when comm is not null then sal + comm
                end) as salario_act,
                (case
                    when comm is null then sal + sal * 0.6
                    when comm is not null then sal * 0.6 + sal + comm
                    end) as salario_next,
                    (case
                        when comm is null then (sal + sal * 0.6) * 0.7 + sal
                        when comm is not null then (sal * 0.6 + sal + comm)* 0.7 + sal + comm
                        end) as salario_nextx2
                    from emp;

---46------

select ename, hiredate from emp where job not like 'SALESMAN';

---47------

select * from emp where empno in (7844, 7900, 7521, 7521, 7782, 7934, 7678,7369) and empno not in (7902, 7839, 7499, 7878);

---48------

select * from emp order by deptno, empno desc;

---49------

select * from emp where empno < mgr and (sal < 2000 and sal > 1000 or deptno = 30);

---50------

select * from emp order by deptno, empno desc;

---51------

select max(sal) as salMax, sum(comm) as commTotal, count(*) as empNum from emp ;

---52------

select ename, job, sal from emp, (select sal as alenSal from emp where ename = 'ALLEN' and rownum = 1) where alenSal <= sal;
                            
---53------

select ename from emp where rownum = 1 order by ename desc ;

---54------

select max(sal) as salMax, min(sal) as commTotal, max(sal) - min(sal) as diff from emp ;

---55------

select avg(sal), dept.dname from emp inner join dept on emp.deptno = dept.deptno where emp.sal < 5000 group by dept.dname having min(sal) > 900;

---56------

select emp.* from emp join dept on emp.deptno = dept.deptno where dept.location like '_______%' order by dept.location desc, emp.ename asc;

---57------

select * from emp where sal >= (select avg(sal) from emp where ROWNUM = 1);

---58------

select * from emp e inner join 
    (select avg(sal) as media, e.deptno from emp e inner join dept d on e.deptno = d.deptno 
    group by e.deptno) s on s.deptno = e.deptno where media < e.sal;

---59------

select count(e.ename) as empno, count(distinct e.job) as jobno, count(distinct e.sal) as salno, sum(DISTINCT e.sal) as salSum from emp e group by e.deptno having e.deptno = 30;

---60------

select count(e.comm) from emp e where e.comm is not null;

---61------

select count(*) from emp e where e.deptno = 20;

---62------

select e.deptno , count(*) from emp e group by e.deptno having count(*) > 3;

---63------

select * from emp e where e.job in (select e.job from dept d inner join emp e on d.deptno = e.deptno where d.dname = 'Sales') and e.deptno = 10;

---64------

select e2.* from emp e1 inner join emp e2 on e1.ename = e2.ename;

---65------

select * from emp e where e.job in (
    select e.job from emp e inner join dept d on e.deptno = d.deptno where d.location = 'Chicago'
);

---66------

select  e.job, count(*) from emp e group by e.job;

---67------

select e.deptno, sum(e.sal) from emp e group by e.deptno;

---68------

select d.dname from dept d inner join emp e on d.deptno = e.deptno group by d.dname having count(e.ename) = 0;

---69------

--creo que esta repetida

---70------

select d.dname, count(e.ename), avg(e.sal) from emp e inner join dept d on e.deptno = d.deptno group by d.dname;

---71------

select * from emp e inner join dept d on e.deptno = d.deptno where d.dname = 'Dallas' or dname = 'New York'


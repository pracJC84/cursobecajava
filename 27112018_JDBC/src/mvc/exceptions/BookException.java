package mvc.exceptions;

public class BookException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	
	public BookException(String mensaje, Throwable excepcion) {
		super(mensaje, excepcion);
	}

	public BookException(String mensaje) {
		super(mensaje);
	}

}

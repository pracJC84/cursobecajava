package mvc.exceptions;

public class AutorException extends Exception{



	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	
	public AutorException(String mensaje, Throwable excepcion) {
		super(mensaje, excepcion);
	}

	public AutorException(String mensaje) {
		super(mensaje);
	}

}

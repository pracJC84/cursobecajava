package mvc.exceptions;

import java.sql.SQLException;

public class ClientException extends Exception{

	public ClientException(String string, SQLException e) {
		super(string, e);
	}

	public ClientException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	
	

}

package mvc.service.impl;

import java.util.List;

import mvc.exceptions.AutorException;
import mvc.exceptions.BookException;
import mvc.model.dao.interfaze.AutorDAO;
import mvc.model.dao.interfaze.BookDAO;
import mvc.model.dto.AutorDTO;
import mvc.model.dto.BookDTO;
import mvc.model.dto.CoverDTO;
import mvc.service.interfaze.BookService;

public class BookServiceImpl implements BookService {

	BookDAO bdao;
	AutorDAO adao;
	
	public BookServiceImpl(BookDAO bdao, AutorDAO adao) {
		this.bdao = bdao;
		this.adao = adao;
	}

	@Override
	public List<BookDTO> getBooksByAutor(int autorID) throws BookException {
		
		return bdao.readByAutor(autorID);
	}

	@Override
	public void addBook(BookDTO book) throws BookException, AutorException {
		
		AutorDTO autorDTO;
		try{
			adao.read(book.getAutor());
		}catch(Exception e) {
			autorDTO = new AutorDTO(54, "anonimo", "anonimo");
			book.setAutor(autorDTO.getId());
			bdao.createBookAndAuthor(book, autorDTO);
		}
			
		
		bdao.create(book);
	}

	@Override
	public List<BookDTO> getAll() throws BookException {
		
		return bdao.readAll();
	}

	@Override
	public CoverDTO getCover(int i) throws BookException {
		
		return bdao.readCover(i);
	}

	@Override
	public void deleteBook(int bookId) throws BookException {
		bdao.delete(bookId);
		
	}
	
	

}

package mvc.service.impl;

import mvc.exceptions.CoverException;
import mvc.model.dao.interfaze.CoverDAO;
import mvc.service.interfaze.CoverService;

public class CoverServiceImpl implements CoverService{

	CoverDAO dao;
	
	
	public CoverServiceImpl(CoverDAO rdao) {
		this.dao = rdao;
	}


	@Override
	public String getCover(int bookID) throws CoverException {
		return dao.read(bookID).getFile();
	}

}

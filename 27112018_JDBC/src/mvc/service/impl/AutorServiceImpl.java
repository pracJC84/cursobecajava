package mvc.service.impl;

import java.util.List;

import mvc.exceptions.AutorException;
import mvc.model.dao.interfaze.AutorDAO;
import mvc.model.dto.AutorDTO;
import mvc.service.interfaze.AutorService;

public class AutorServiceImpl implements AutorService {

	private AutorDAO authorDAO;
	
	public AutorServiceImpl(AutorDAO adao) {
		this.authorDAO = adao;
	}

	@Override
	public List<AutorDTO> getAll() throws AutorException {
		return authorDAO.readAll();
	}

	@Override
	public void delete(int id) throws AutorException {
		this.authorDAO.delete(id);
		
	}
}

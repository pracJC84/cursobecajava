package mvc.service.interfaze;

import java.util.List;

import mvc.exceptions.AutorException;
import mvc.model.dto.AutorDTO;

public interface AutorService {

	/**
	 * 
	 * @return all the authors in the system
	 * @throws AutorException 
	 */
	public List<AutorDTO> getAll() throws AutorException;

	public void delete(int id) throws AutorException;
	
}

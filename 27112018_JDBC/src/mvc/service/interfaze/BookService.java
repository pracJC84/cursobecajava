package mvc.service.interfaze;


import java.util.List;

import mvc.exceptions.AutorException;
import mvc.exceptions.BookException;
import mvc.model.dto.BookDTO;
import mvc.model.dto.CoverDTO;

public interface BookService {
	

	/**
	 * 
	 * @param autorID
	 * @return books that a author had write
	 * @throws BookException 
	 */
	public List<BookDTO> getBooksByAutor(int autorID) throws BookException;
	/**
	 * 
	 * @param book
	 * @throws BookException 
	 * @throws AutorException 
	 */
	public void addBook(BookDTO book) throws BookException, AutorException;
	
	public List<BookDTO> getAll() throws BookException;
	
	public CoverDTO getCover(int i) throws BookException;
	
	public void deleteBook(int bookId) throws BookException;
	
}

package mvc.model.dto;

import java.io.Serializable;

public class BookDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	private int id, ref, autor, cover;
	private String title, date;
	public int getRef() {
		return ref;
	}
	public void setRef(int ref) {
		this.ref = ref;
	}
	public int getAutor() {
		return autor;
	}
	public void setAutor(int autor) {
		this.autor = autor;
	}
	public int getCover() {
		return cover;
	}
	public void setCover(int cover) {
		this.cover = cover;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public BookDTO(int id, String title, int ref,  String date, int autor, int cover) {
		super();
		this.id = id;
		this.ref = ref;
		this.autor = autor;
		this.cover = cover;
		this.title = title;
		this.date = date;
	}
	@Override
	public String toString() {
		return "BookDTO [id=" + id + ", ref=" + ref + ", autor=" + autor + ", cover=" + cover + ", title=" + title
				+ ", date=" + date + "]";
	}
}

package mvc.model.dto;

import java.io.Serializable;

public class AutorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 666L;
	
	private int id;
	private String name, forename;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForename() {
		return forename;
	}
	public void setForename(String forename) {
		this.forename = forename;
	}
	public int getId() {
		return id;
	}
	public AutorDTO(int id, String name, String forename) {
		super();
		this.id = id;
		this.name = name;
		this.forename = forename;
	}
	@Override
	public String toString() {
		return "AutorDTO [id=" + id + ", name=" + name + ", forename=" + forename + "]";
	}
}

package mvc.model.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mvc.exceptions.AutorException;
import mvc.model.dao.interfaze.AutorDAO;
import mvc.model.dto.AutorDTO;
import utilities.JDBC;

public class AutorDAOImpl implements AutorDAO{
	
	private static final String SELECT = "SELECT * FROM Autor WHERE aid = ?";
	private static final String INSERT = "INSERT INTO Autor VALUES (?, ?, ?)";
	private static final String UPDATE = "UPDATE Autor SET name = ?, forename = ?";
	private static final String DELETE = "DELETE FROM Autor WHERE aid = ?";
	private static final String SELECT_ALL = "SELECT * FROM autor";

	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}
	
	@Override
	public void create(AutorDTO dto) throws AutorException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getName());
			ps.setString(3, dto.getForename());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new AutorException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new AutorException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public AutorDTO read(int idDTO) throws AutorException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setInt(1, idDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id = rs.getInt("aid");
				String name = rs.getString("name");
				String forename = rs.getString("forename");

				return new AutorDTO(id, name, forename);

			} else {
				throw new AutorException("No hay Autor con id " + idDTO);
			}

		} catch (SQLException e) {
			throw new AutorException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new AutorException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(AutorDTO dto) throws AutorException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setString(1, dto.getName());
			ps.setString(2, dto.getForename());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new AutorException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new AutorException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(int idDTO) throws AutorException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setInt(1, idDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new AutorException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new AutorException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<AutorDTO> readAll() throws AutorException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<AutorDTO> Autors = new ArrayList<>();

				int id;
				String name = null, forename = null;
			
				do {
					id = rs.getInt("aid");
					name = rs.getString("name");
					forename = rs.getString("forename");

					Autors.add(new AutorDTO(
							id, name, forename));

				} while (rs.next());

				return Autors;

			} else {
				throw new AutorException("No hay Autors");
			}

		} catch (SQLException e) {
			throw new AutorException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new AutorException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}
}

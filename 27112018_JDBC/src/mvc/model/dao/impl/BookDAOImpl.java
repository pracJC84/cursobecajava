package mvc.model.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mvc.exceptions.BookException;
import mvc.model.dao.interfaze.BookDAO;
import mvc.model.dto.AutorDTO;
import mvc.model.dto.BookDTO;
import mvc.model.dto.CoverDTO;
import utilities.JDBC;

public class BookDAOImpl implements BookDAO{

	
	private static final String SELECT = "SELECT * FROM Book WHERE bid = ?";
	private static final String INSERT = "INSERT INTO Book VALUES (?, ?, ?, ?, ?, ?)";
	private static final String INSERT_AUTHOR = "INSERT INTO Autor VALUES (?, ?, ?)";
	private static final String UPDATE = "UPDATE Book SET name = ?, forename = ?";
	private static final String DELETE = "DELETE FROM Book WHERE bid = ?";
	private static final String SELECT_ALL = "SELECT * FROM Book";
	private static final String SELECT_ALL_BY_AUTOR = "SELECT * FROM Book where fk_autor = ?";

	
	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);
	}
	
	@Override
	public void create(BookDTO dto) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(INSERT);

			ps.setInt(1, dto.getId());
			ps.setString(2, dto.getTitle());
			ps.setInt(3, dto.getRef());
			ps.setString(4, dto.getDate());
			ps.setInt(5, dto.getAutor());
			ps.setInt(6, dto.getCover());
			
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new BookException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}

	@Override
	public BookDTO read(String idDTO) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT);

			ps.setString(1, idDTO);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				int id = rs.getInt("aid");
				String title = rs.getString("name");
				int ref = rs.getInt("forename");
				String date = rs.getString("rdate");
				int autor =  rs.getInt("fk_autor");
				int cover = rs.getInt("fk_cover");

				return new BookDTO(id, title, ref, date, autor, cover);

			} else {
				throw new BookException("No hay Autor con id " + idDTO);
			}

		} catch (SQLException e) {
			throw new BookException("Fallo lectura en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre lectura en BBDD", e);
				}
			}
		}
	}

	@Override
	public void update(BookDTO dto) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(UPDATE);

			ps.setString(1, dto.getTitle());
			ps.setInt(2, dto.getRef());
			ps.setString(3, dto.getDate());
			ps.setInt(4, dto.getAutor());
			ps.setInt(5, dto.getCover());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new BookException("Fallo actualizacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre actualizacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public void delete(int idDTO) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(DELETE);

			ps.setInt(1, idDTO);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new BookException("Fallo eliminacion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre eliminacion en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<BookDTO> readAll() throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<BookDTO> Autors = new ArrayList<>();
			
				do {
					int id = rs.getInt("bid");
					String title = rs.getString("title");
					int ref = rs.getInt("ref");
					String date = rs.getString("rdate");
					int autor =  rs.getInt("fk_autor");
					int cover = rs.getInt("fk_cover");

					Autors.add(new BookDTO(id, title, ref, date, autor, cover));

				} while (rs.next());

				return Autors;

			} else {
				throw new BookException("No hay books");
			}

		} catch (SQLException e) {
			throw new BookException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

	@Override
	public List<BookDTO> readByAutor(int autorID) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL_BY_AUTOR);
			
			ps.setInt(1, autorID);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				List<BookDTO> Autors = new ArrayList<>();
			
				do {
					int id = rs.getInt("bid");
					String title = rs.getString("title");
					int ref = rs.getInt("ref");
					String date = rs.getString("rdate");
					int autor =  rs.getInt("fk_autor");
					int cover = rs.getInt("fk_cover");

					Autors.add(new BookDTO(id, title, ref, date, autor, cover));

				} while (rs.next());

				return Autors;

			} else {
				throw new BookException("No hay books");
			}

		} catch (SQLException e) {
			throw new BookException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

	@Override
	public CoverDTO readCover(int bookID) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();

			PreparedStatement ps = cn.prepareStatement(SELECT_ALL_BY_AUTOR);
			
			ps.setInt(1, bookID);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
			
				int id = rs.getInt("cid");
				String file = rs.getString("cfile");
				String name = rs.getString("cname");
				
				return new CoverDTO(id, file, name);

			} else {
				throw new BookException("No hay books");
			}

		} catch (SQLException e) {
			throw new BookException("Fallo lectura completa en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre lectura completa en BBDD", e);
				}
			}
		}
	}

	@Override
	public void createBookAndAuthor(BookDTO bookDTO, AutorDTO autorDTO) throws BookException {
		Connection cn = null;
		try {
			cn = getConnection();
			
			cn.setAutoCommit(false);
			
			PreparedStatement ps = cn.prepareStatement(INSERT_AUTHOR);

			ps.setInt(1, autorDTO.getId());
			ps.setString(2, autorDTO.getName());
			ps.setString(3, autorDTO.getForename());
			
			ps.executeUpdate();
			
			
			PreparedStatement ps2 = cn.prepareStatement(INSERT);

			ps2.setInt(1, bookDTO.getId());
			ps2.setString(2, bookDTO.getTitle());
			ps2.setInt(3, bookDTO.getRef());
			ps2.setString(4, bookDTO.getDate());
			ps2.setInt(5, bookDTO.getAutor());
			ps2.setInt(6, bookDTO.getCover());
			
			ps2.executeUpdate();
			
			cn.commit();

		} catch (SQLException e) {
			throw new BookException("Fallo insercion en BBDD", e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					throw new BookException("Fallo cierre insercion en BBDD", e);
				}
			}
		}
	}
}


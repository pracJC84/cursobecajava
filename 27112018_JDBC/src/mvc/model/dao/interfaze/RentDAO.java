package mvc.model.dao.interfaze;

import java.util.List;

import mvc.exceptions.RentException;
import mvc.model.dto.BookDTO;
import mvc.model.dto.RentDTO;

public interface RentDAO {
	/*
	 * Crear DTO (INSERT en BBDD)
	 */
	void create(RentDTO dto) throws RentException;
	
	/*
	 * Recuperar DTO (SELECT con CLAUSULA WHERE en BBDD)
	 */
	RentDTO read(String idDTO) throws RentException;
	
	/*
	 * Modificar DTO (UPDATE en BBDD)
	 */
	void update(int rentID) throws RentException;
	
	/*
	 * Eliminar DTO (DELETE en BBDD)
	 */
	void delete(int idDTO) throws RentException;
	
	List<BookDTO> readPendents(int clientID) throws RentException;
	
	/*
	 * Recuperar TODOS los DTO (SELECT en BBDD)
	 */
	List<RentDTO> readAll() throws RentException;
	
	/*
	 * rent a book given a clientid and a Rentid
	 */

	void rentBook(int clientID, int bookID);
}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%-- Libreria core de JSTL --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- Libreria para formularios HTML de Spring MVC --%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring MVC</title>
</head>
<body>
	<h2>Bienvenido ${usuario.user}</h2>
	<h3>Agregar Alumno</h3>
	
	<%-- Crear variable donde guardar la pagina a la que ir --%>
	<c:url var="peticion" value="listadoAlumnos.html" />

	
	<%-- 
		 Crear formulario de Spring para enlazar los controles
	 	 con las propiedades del objeto del modelo que se 
	 	 ha enviado (usuario) 
	 	 Atributo action tiene el patron de llamada al metodo
	 	 de negocio del controlador que se quiere ejecutar
	--%>
	<form:form id="list" modelAttribute="alumnoForm" method="post"
		action="${peticion}">
	
		<table style="width: 350px; height: 125px">
			<tr>
				<td>
					<%-- Etiqueta de texto asociada a la
						 caja de nombre --%>
					<form:label path="nombre">Nombre</form:label>
				</td>
				<td>
					<%-- Caja de texto asociada a la
						 propiedad nombre del formbean asociado --%>
					<form:input path="nombre" />
				</td>
			</tr>
			<tr>
				<td>
					<%-- Etiqueta de texto asociada a la
						 caja de email --%>
					<form:label path="email">Email</form:label>
				</td>
				<td>
					<%-- Caja de texto asociada a la
						 propiedad email del formbean asociado --%>
					<form:input path="email" />
				</td>
			</tr>
			<tr>
				<td>
					<%-- Etiqueta de texto asociada a la
						 caja de telefono --%>
					<form:label path="telefono">Telefono</form:label>
				</td>
				<td>
					<%-- Caja de texto asociada a la
						 propiedad email del formbean asociado --%>
					<form:input path="telefono" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<%-- Boton de envio --%>
					<input type="submit" value="   Crear Usuario   "/>
				</td>
			</tr>
		</table>
	
	</form:form>
</body>
</html>
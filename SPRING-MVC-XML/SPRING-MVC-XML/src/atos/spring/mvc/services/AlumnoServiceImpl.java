package atos.spring.mvc.services;

import java.util.ArrayList;
import java.util.List;

import atos.spring.mvc.modelo.AlumnoForm;

public class AlumnoServiceImpl implements AlumnoService {

	/*
	 * Lista con datos locales para el ejemplo
	 */
	private static List<AlumnoForm> students = new ArrayList<>();
	
	@Override
	public void add(AlumnoForm alumno) {
		students.add(alumno);	
	}

	@Override
	public List<AlumnoForm> getAll() {
		return students;
	}

	@Override
	public AlumnoForm getAlumno(String id) {
		for(AlumnoForm a : students)
			if(a.getNombre().equals(id))
				return a;
		return null;
	}

	@Override
	public void updateAlumno(AlumnoForm alumno) {
		for(AlumnoForm a : students)
			if(a.getNombre() == alumno.getNombre()) {
				a.setEmail(alumno.getEmail());
				a.setTelefono(alumno.getTelefono());
			}
				
	}

}

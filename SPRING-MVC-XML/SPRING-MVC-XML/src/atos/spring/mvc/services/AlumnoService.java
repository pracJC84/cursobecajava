package atos.spring.mvc.services;

import java.util.List;

import atos.spring.mvc.modelo.AlumnoForm;

/*
 * Encapsular trabajo con datos SIN DAO porque NO hay BBDD
 */
public interface AlumnoService {

	void add(AlumnoForm alumno);
	
	List<AlumnoForm> getAll();

	AlumnoForm getAlumno(String id);

	void updateAlumno(AlumnoForm alumno);
	
}

package curso.utilidades;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

public class Datos {

	public static void infoColumnas(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		// Mostrar en pantalla el n�mero de columnas del ResultSet
		System.out.println("N�mero de columnas: " + metaData.getColumnCount());

		// Mostrar en pantalla el nombre de columnas del ResultSet
		System.out.println("Nombre de las columnas");
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			System.out.println("\t" + metaData.getColumnName(indice));
		}

		// Mostrar en pantalla el alias de columnas del ResultSet
		System.out.println("Alias de las columnas");
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			System.out.println("\t" + metaData.getColumnLabel(indice));
		}

		// Mostrar en pantalla el n�mero de columnas del ResultSet
		// que son de tipo cadena (VARCHAR2)
		int total = 0;
		for (int indice = 1; indice <= metaData.getColumnCount(); indice++) {
			if(metaData.getColumnType(indice)==Types.VARCHAR) {
				total++;
			}
		}
		System.out.println("N�mero de columnas de tipo cadena: " +
				total);

	}

}

package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import curso.utilidades.Datos;
import curso.utilidades.JDBC;

/*
 * Información columnas de ResultSet
 */
public class Test3 {

	public static void main(String[] args) {

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);

		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");

			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;

		// Ejcucion instruccioneds parametrizadas
		PreparedStatement ps = null;

		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);

			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			ps = connection.prepareStatement(
					"SELECT REGION_ID CODIGO, REGION_NAME REGION "
					+ "FROM HR.REGIONS");

			ResultSet rs = ps.executeQuery();

			Datos.infoColumnas(rs);

		} catch (SQLException e1) {
			
			e1.printStackTrace();

		} finally {

			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}

}

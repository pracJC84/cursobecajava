package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import curso.utilidades.JDBC;

/*
 * Gestion de transacciones
 * 
 * Realizar pedido para la compra de 4 productos
 * 
 */
public class Test {

	public static void main(String[] args) {

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);

		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");

			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;

		// Ejcucion instruccioneds parametrizadas
		PreparedStatement ps = null;

		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);

			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			ps = connection.prepareStatement(
					"INSERT INTO detalles_pedido (id_pedido,id_detalle,id_producto,unidades) VALUES (?,?,?,?)");

			// Deshabilitar modo autoconfirmación PARA
			// CREAR UNA TRANSACCION POR CODIGO
			connection.setAutoCommit(false);
			
			// Primer producto
			ps.setInt(1, 43); // CODIGO PEDIDO
			ps.setInt(2, 1); // CODIGO DETALLE PEDIDO
			ps.setInt(3, 4); // CODIGO PRODUCTO
			ps.setInt(4, 2); // UNIDADES COMPRADAS

			ps.executeUpdate();

			ps.setInt(1, 43); // CODIGO PEDIDO
			ps.setInt(2, 2); // CODIGO DETALLE PEDIDO
			ps.setInt(3, 22); // CODIGO PRODUCTO
			ps.setInt(4, 4); // UNIDADES COMPRADAS

			ps.executeUpdate();

			ps.setInt(1, 43); // CODIGO PEDIDO
			ps.setInt(2, 3); // CODIGO DETALLE PEDIDO
			ps.setInt(3, 7); // CODIGO PRODUCTO
			ps.setInt(4, 1); // UNIDADES COMPRADAS

			ps.executeUpdate();
			connection.rollback();
			
			ps.setInt(1, 43); // CODIGO PEDIDO
			ps.setInt(2, 4); // CODIGO DETALLE PEDIDO
			ps.setInt(3, 2); // CODIGO PRODUCTO
			ps.setInt(4, 3); // UNIDADES COMPRADAS

			ps.executeUpdate();

			// Confirmar la transacción
			connection.commit();
			
			System.out.println("Se ha registrado el pedido!!!!!");

		} catch (SQLException e1) {
			// Descartar la transacción
			try {
				connection.rollback();
				
				System.out.println("No se ha registrado el pedido!!!!!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// TODO Auto-generated catch block
			e1.printStackTrace();

		} finally {

			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}

}

package cuenta;

public class Cuenta {
	
	
	private String titular;
	private double cantidad;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cuenta c = new Cuenta("pedro", 100.1);
		System.out.println(c.toString());
		c.ingresar(21);
		System.out.println(c.toString());
		c.retirar(4.2);
		System.out.println(c.toString());
	}

	
	public Boolean ingresar(double cantidad) {
		
		//precondicion
		if(cantidad < 0) 
			return false;
		
		//body
		this.setCantidad(this.getCantidad() + cantidad);
		return true;
		
	}
	
	public Boolean retirar(double cantidad) {
		double actual = this.getCantidad() - cantidad;
		
		//precondicion
		if(cantidad < 0 || actual < 0) 
			return false;
		
		//body
		this.setCantidad(actual);
		return true;
		
	}
	
	//-------------------------------------------------
	//----------Constructores----------------------
	//-------------------------------------------------	
	
	public Cuenta(String titular) {
		this.titular = titular;
		this.cantidad = 0;
	}
	
	public Cuenta(String titular, double cantidad ) {
		this.titular = titular;
		this.cantidad = cantidad;
	}


	//-------------------------------------------------
	//-----------Getters and setters-------------------
	//------------------------------------------------
	public String getTitular() {
		return titular;
	}
	
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public double getCantidad() {
		return cantidad;
	}

	@Override
	public String toString() {
		return "Cuenta [titular=" + titular + ", cantidad=" + cantidad + "]";
	}

	

}

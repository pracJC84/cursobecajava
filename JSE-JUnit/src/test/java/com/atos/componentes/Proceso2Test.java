package com.atos.componentes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * Clase para realizar pruebas unitarias del componente Proceso2
 * mediante JUnit
 */
public class Proceso2Test {

	private static final int NEW_DEFAULT = 
			Proceso2.DEFAULT + 1;
	
	// Componente a comprobar
	private Proceso2 proceso;
	
	/*
	 * Ejecutar justo antes de ejecutar cada metodo de testing
	 */
	@Before
	public void inicializarComponente() {
		proceso = new Proceso2();
	}
	
	/*
	 * Ejecutar justo despues de ejecutar cada metodo de testing
	 */
	@After
	public void finalizarComponente() {
		proceso.finalizar();
	}
	
	/*
	 * Caso de prueba o test para el metodo setNumeroEjecuciones
	 * 
	 * Métodos publicos con la anotación @Test
	 */
	@Test
	public void setNumeroEjecuciones() {
		// 1.- Arrange => Creacion del componente a testear
		// Creado en el metodo con anotacion @Before
		
		// 2.- Execution => Fase de ejecución
		proceso.setNumeroEjecuciones(NEW_DEFAULT);
				
		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertEquals(NEW_DEFAULT, proceso.getNumeroEjecuciones());
		
	}
	
	@Test
	public void estadoInicial() {
		// Comprobar que una condicion es verdadera
		// Caso de no serlo lanzamos error
		assertTrue(proceso.getNumeroEjecuciones() > 0);
	}
	
	@Test
	public void asignacionInferior() {
		int original = proceso.getNumeroEjecuciones();
		
		proceso.setNumeroEjecuciones(Proceso2.LOWER_BOUND - 1);
		
		assertEquals(original, proceso.getNumeroEjecuciones());
	}
	
	@Test
	public void asignacionSuperior() {
		int original = proceso.getNumeroEjecuciones();
		
		proceso.setNumeroEjecuciones(Proceso2.UPPER_BOUND + 1);
		
		assertEquals(original, proceso.getNumeroEjecuciones());
	}
}

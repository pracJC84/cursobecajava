package com.atos.componentes;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MatematicasTest2 {

	
	Matematicas mat;
	@Before
	public void inicializarComponente() {
		mat = new Matematicas();
	}
	
	@Test
	public void testMat() {
		double res = mat.multiplicacion(2.0, 2.0);
		
		assertEquals(res, 4.0, 0.001);
	}
	
}

package com.atos.componentes;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/*
 * Clase para realizar pruebas unitarias del componente 
 * CategoriaDAOImpl mediante JUnit pero utilizando conjunto
 * de datos como muestra para multiples ejecuciones
 * 
 * Los test se ejcutar�n tantas veces como parametros de 
 * entrada se hayan configurado
 * 
 * @RunWith => Integrar los test con algun framework de pruebas
 * 
 * @RunWith => Ejecutar test parametrizados mediante la clase
 * Parameterized
 * 
 */
// @RunWith(value=Arquilian.class)
// @RunWith(value=Sellenium.class)

/*
 * Ejecucion parametrizada de Test JUnit
 */
@RunWith(value=Parameterized.class)
public class CategoriaDAOImplParametrosTest {

	// Componente a comprobar
	private CategoriaDAO dao;

	/*
	 * Ejecutar justo antes de ejecutar cada metodo de testing
	 */
	@Before
	public void setUp() {
		// 1.- Arrange => Creacion del componente a testear
		dao = new CategoriaDAOImpl();
	}

	/*
	 * Crear un campo para cada parametro del metodo de negocio del componente
	 * (getCategory en este caso) que se quiere comprobar
	 */
	private int codigo;

	/*
	 * Constructor general para inicializar los campos que se enlazar�n a los
	 * parametros
	 */
	public CategoriaDAOImplParametrosTest(int codigo) {
		this.codigo = codigo;
	}

	/*
	 * Crear metodo publico y estatico que devuelve una coleccion
	 * (objeto Iterable) para matriz de una dimension de tipo Object
	 * 
	 * Esta matriz tiene tantos elementos como campos (parametros)
	 * hay
	 * 
	 * La coleccion tiene tantos elementos como veces se quiera
	 * probar el metodo de negocio
	 * 
	 * El metodo lleva OBLIGATORIAMENTE la anotacion @Parameters
	 */
	@Parameters
	public static Collection<Object []> getData() {
		Object [][] parametros = {
				{ 2 }, // Primera ejecucion
				{ 4 }, // Segunda ejecucion
				{ 9 }, // Tercera ejecucion
				{ 6 }  // Cuarta ejecucion
		};
		
		return Arrays.asList(parametros);
	}
	
	
	/*
	 * Caso de prueba o test para el metodo getCategory
	 * 
	 * M�todos publicos con la anotaci�n @Test
	 */
	@Test
	public void pruebaCategoriaOK() throws CategoriaException {
		// 2.- Execution => Fase de ejecuci�n
		Categoria categoria = dao.getCategory(codigo);

		// 3.- Validity => Fase de comprobaci�n
		// Comprobar el resultado. Utilizaci�n de aserciones
		// de JUnit
		assertEquals(codigo, categoria.getCodigo());

	}

}

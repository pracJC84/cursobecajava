package com.atos.componentes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/*
 * Clase para realizar pruebas unitarias del componente 
 * CategoriaDAOImpl mediante JUnit
 */
public class CategoriaDAOImplTest {

	// Componente a comprobar
	private CategoriaDAO dao;

	/*
	 * Ejecutar justo antes de ejecutar cada metodo de testing
	 */
	@Before
	public void setUp() {
		// 1.- Arrange => Creacion del componente a testear
		dao = new CategoriaDAOImpl();
	}

	/*
	 * Caso de prueba o test para el metodo getCategories
	 * 
	 * Métodos publicos con la anotación @Test
	 */
	@Test
	public void pruebaListaOK() throws CategoriaException {
		// 2.- Execution => Fase de ejecución
		List<Categoria> categorias = dao.getCategories();

		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertEquals(categorias, new ArrayList<>());

	}

	@Test
	public void pruebaListaOK2() throws CategoriaException {
		// 2.- Execution => Fase de ejecución
		List<Categoria> categorias = dao.getCategories();

		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertTrue(categorias.isEmpty());

	}
	
	/*
	 * Caso de prueba o test para el metodo getCategory
	 * 
	 * Métodos publicos con la anotación @Test
	 */
	@Test
	public void pruebaCategoriaOK() throws CategoriaException {
		// 2.- Execution => Fase de ejecución
		int codigo = 3;
		Categoria categoria = dao.getCategory(codigo);

		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertEquals(codigo, categoria.getCodigo());

	}
	
	/*
	 * Caso de prueba o test para el metodo getCategory
	 * 
	 * Comprobar lanzamiento de excepcion mediante atributo
	 * expected de @Test
	 *  
	 * Métodos publicos con la anotación @Test
	 */
	@Test(expected=CategoriaException.class)
	public void pruebaCategoriaOK2() throws CategoriaException {
		// 2.- Execution => Fase de ejecución
		int codigo = 30;
		Categoria categoria = dao.getCategory(codigo);

		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertEquals(codigo, categoria.getCodigo());

	}
}

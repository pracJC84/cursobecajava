package com.atos.componentes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/*
 * Crear una suite o conjunto de clases para testing
 * 
 * Ejecucion a la vez de multiples test unitarios contenidos
 * en multiples clases
 * 
 * Ejecutar (Runner) con  la clase Suite de JUnit
 */
@RunWith(value=Suite.class)
/*
 * Especificar las clases de testing (casos de prueba) que
 * componen la suite (conjunto de pruebas)
 */
@SuiteClasses({
	ProcesoTest.class,
	Proceso2Test.class,
	CategoriaDAOImplTest.class,
	CategoriaDAOImplParametrosTest.class
})
public class SuiteJUnit {

}

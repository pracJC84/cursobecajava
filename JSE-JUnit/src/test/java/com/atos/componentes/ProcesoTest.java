package com.atos.componentes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/*
 * Clase para realizar pruebas unitarias del componente Proceso
 * mediante JUnit
 */
public class ProcesoTest {

	/*
	 * Caso de prueba o test para el metodo setNumeroEjecuciones
	 * 
	 * Métodos publicos con la anotación @Test
	 */
	@Test
	public void setNumeroEjecuciones() {
		// 1.- Arrange => Creacion del componente a testear
		Proceso proceso = new Proceso();
		
		// 2.- Execution => Fase de ejecución
		int veces = 5;
		proceso.setNumeroEjecuciones(veces);
		int result = proceso.getNumeroEjecuciones();
		
		// 3.- Validity => Fase de comprobación
		// Comprobar el resultado. Utilización de aserciones
		// de JUnit
		assertEquals(result, 5);
		
	}
}

package com.atos.componentes;

import java.util.List;

public interface CategoriaDAO {

	List<Categoria> getCategories() throws CategoriaException;
	
	Categoria getCategory(int codigo) throws CategoriaException;
	
}

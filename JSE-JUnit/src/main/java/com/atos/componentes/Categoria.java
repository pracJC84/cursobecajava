package com.atos.componentes;

import java.io.Serializable;

public class Categoria implements Serializable {

	private static final long serialVersionUID = 666L;

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}

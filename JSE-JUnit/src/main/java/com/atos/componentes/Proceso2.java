package com.atos.componentes;

public class Proceso2 {

	public static final int LOWER_BOUND = 1;
	public static final int UPPER_BOUND = 30;
	public static final int DEFAULT = 15;
	
	private int numeroEjecuciones;

	public Proceso2() {
		this.numeroEjecuciones = DEFAULT;
	}
	
	public int getNumeroEjecuciones() {
		return numeroEjecuciones;
	}

	public void setNumeroEjecuciones(int numeroEjecuciones) {
		if(numeroEjecuciones >= LOWER_BOUND && 
				numeroEjecuciones <= UPPER_BOUND) {
			this.numeroEjecuciones = numeroEjecuciones;
		}
	}

	public void finalizar() {
		System.out.println("Liberar recursos cuando finaliza "
				+ "la utilizacion del objeto");
	}
	
}

create table autor(
    aID number not null,
    name varchar (32) not null,
    forename varchar (32) not null,
    primary key (aID)
);

create table book(
    bID number not null,
    title varchar(32) not null,
    ref number not null,
    rDate varchar(12),
    autor number  not null,
    PRIMARY KEY (bID),
    FOREIGN key (autor) references Autor(aID)
);


insert into autor values(
    1,  'laura', 'gallego garcia'
);

insert into autor values(
    2, 'patrick', 'rothfuss'
);

insert into book values (
    1, 'donde cantan los arboles', 111111, '2012-06-17', 1
);

insert into book values (
    2, 'el nombre del viento', 111112, '2014-06-17', 2
);

insert into book values (
    3, 'el temor de un hombre sabio', 111113, '2016-06-17', 2
);

--select book.* from book inner join autor on book.autor = autor.aID where 'laura' = autor.name

--select * from book;








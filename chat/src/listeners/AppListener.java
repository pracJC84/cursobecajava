package listeners;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/*
 * Listener (gestor de eventos) para aplicacion (ServletContext)
 */
@WebListener
public class AppListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Replegada del servidor la app");
	}

	public void contextInitialized(ServletContextEvent sce) {
		// Recuperar app que se acaba de crear
		ServletContext context = sce.getServletContext();

		List<String> cadenas = new ArrayList<>();

		cadenas.add("<h1>Mensajeria<h1>");

		// Crear atributo de app para lista de cadenas
		context.setAttribute("log", cadenas);
	}

}

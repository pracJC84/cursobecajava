package controller.actions;

import javax.servlet.http.HttpServletRequest;

public class ChangeFormAction implements Action {

	@Override
	public String execute(HttpServletRequest resquest) {
		
		return "/changeForm.jsp";
	}

}

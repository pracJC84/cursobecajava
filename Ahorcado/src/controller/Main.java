package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.actions.Action;
import controller.actions.ChangeFormAction;
import controller.actions.ChangeWord;
import controller.actions.NewWordAction;
import controller.actions.PlayAction;
import controller.actions.RestartAction;

@WebServlet("/main")
public class Main extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	protected void service(HttpServletRequest request, HttpServletResponse response) {
		
		Action action = null;
		String myAction = request.getParameter("action");
		
		switch (myAction) {
		case "play":
			action = new PlayAction();
			break;
		case "restart":
			action = new RestartAction();
			break;
		case "newWord":
			action = new NewWordAction();
			break;
		case "changeForm":
			action = new ChangeFormAction();
			break;
		case "changeWord":
			action = new NewWordAction();
			break;
		default:
			System.out.println("CAAAAAAAFREEEEEEEE");
			break;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(action.execute(request));

		// Hacer reenvio de la peticion (y respuesta asociada)
		// a la vista
		try {
			dispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}

package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import curso.utilidades.JDBC;

/*
 * Interfaz PreparedStatement
 * 
 * Ejecucion de instrucciones parametrizadas o precompiladas
 * 
 * Parametros de entrada (?) en base 1 para reemplazar valores
 * NUNCA objetos
 * 
 */
public class Test5 {

	public static void main(String[] args) {

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);

		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");

			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;

		// Ejecucion instrucciones SQL
		Statement statement = null;

		// Ejcucion instruccioneds parametrizadas
		PreparedStatement ps = null;
		
		// Datos para hacer inserciones en la tabla HR.REGIONS
		// Cada fila se corresponde con un nuevo registro
		Object [][] datos = {
				{5, "Antartida"}, // fila
				{6, "Australia"},
				{7, "Espa�a"},
		};
		
		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);

			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			// INSTRUCCIONES SQL => Statement
			statement = connection.createStatement();

			// INSTRUCCIONES SQL PARAMETRIZADAS  => PreparedStatement
			ps = connection.prepareStatement(JDBC.INSERT);
			
			// Recorrer la matriz por filas
			for(Object [] fila : datos) {
				// Recuperar valores
				int codigo = new Integer(fila[0].toString());
				String region = fila[1].toString();
				
				// Dar valor a parametros de entrada
				ps.setInt(1, codigo);
				ps.setString(2, region);
				
				// Ejecutar parametrizada
				ps.executeUpdate();
			}
						
			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			ResultSet rs = statement.executeQuery(JDBC.SELECT);

			// 6.- PROCESAR RESULTADOS EJECUCION
			JDBC.showRegions(rs);
			
			statement.executeUpdate(
					"DELETE FROM HR.regions WHERE REGION_ID > 4");
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();

		} finally {

			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}

}

package curso.ejecucion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

import curso.utilidades.JDBC;

/*
 * Interfaz CallableStatement
 * 
 * Ejecucion de subprogramas de BBDD
 * 
 * Parametros (?) en base 1 para reemplazar valores
 * NUNCA objetos
 * 
 * Antes de ejecutar:
 * 
 * Dar valor a los par�metros de entrada (setTipo)
 * 
 * Registrar par�metros de salida y valor de devoluci�n
 * para funciones (registerOutParameter)
 * 
 * Despues de ejecutar:
 * 
 * Recuperar valor par�metros de salida y valor de devoluci�n
 * para funciones
 * 
 * 
 */
public class Test6 {

	public static void main(String[] args) {

		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);

		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase" + " con los drivers de BBDD");

			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;

		// Ejecucion subprogramas BBDD
		CallableStatement cs = null;

		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(JDBC.URL, JDBC.USER, JDBC.PASSWORD);

			// 4.- CREAR OBJETO DE EJECUCION SUBPROGRAMAS BBDD
			// INSTRUCCIONES SQL => Statement
			// Ejecucion procedimiento HR.TELEFONO_EMPLEADO
			// con parametro de entrada y de otro de salida
			/*
			 * CREATE OR REPLACE PROCEDURE HR.TELEFONO_EMPLEADO ( ID_EMPLEADO IN
			 * HR.EMPLOYEES.EMPLOYEE_ID%TYPE, TELEFONO OUT VARCHAR2 ) AS BEGIN -- RECUPERAR
			 * Y GUARDAR EN EL PARAMETRO DE SALIDA EL TELEFONO -- DEL EMPLEADO CUYO CODIGO
			 * SE PASA COMO PARAMETRO DE ENTRADA SELECT PHONE_NUMBER INTO TELEFONO FROM
			 * HR.EMPLOYEES WHERE EMPLOYEE_ID = ID_EMPLEADO;
			 * 
			 * END TELEFONO_EMPLEADO;
			 */
			cs = connection.prepareCall("{call HR.TELEFONO_EMPLEADO(?, ?)}");

			// 5.- EJECUTAR SUBPROGRAMA SQL SIEMPRE CON
			// EXECUTEUPDATE

			// Valor para el parametro de entrada (por nombre
			// o indice en base 1)
			cs.setInt("ID_EMPLEADO", 107);
			// cs.setInt(1, 107);

			// Registrar parametro de salida indicando el
			// tipo de dato SQL
			cs.registerOutParameter("TELEFONO", Types.VARCHAR);
			// cs.registerOutParameter(2, Types.VARCHAR);

			// Ejecutar subprograma
			cs.executeUpdate();

			// 6.- PROCESAR RESULTADOS EJECUCION
			// Recuperar valor del par�metro de salida
			String telefono = cs.getString("TELEFONO");
			// String telefono = cs.getString(2);

			System.out.println("Tel�fono empleado 107: " + telefono);

			// Ejecucion funcion HR.EMPLEADOS_DEPARTAMENTO
			// con parametro de entrada
			/*
			 * CREATE OR REPLACE FUNCTION HR.EMPLEADOS_DEPARTAMENTO ( ID_DEPARTAMENTO IN
			 * HR.DEPARTMENTS.DEPARTMENT_ID%TYPE ) RETURN NUMBER AS v_Total NUMBER; BEGIN --
			 * RECUPERAR NUMERO DE EMPLEADOS DE UN DETERMINADO DEPARTAMENTO -- SEGUN SU
			 * CODIGO (PARAMETRO DE ENTRADA ID_DEPARTAMENTO) SELECT COUNT(*) INTO v_Total
			 * FROM HR.EMPLOYEES WHERE DEPARTMENT_ID = ID_DEPARTAMENTO;
			 * 
			 * RETURN v_Total;
			 * 
			 * END EMPLEADOS_DEPARTAMENTO;
			 */
			cs = connection.prepareCall("{ ? = call HR.EMPLEADOS_DEPARTAMENTO(?)}");

			// Valor para el parametro de entrada (por nombre
			// o indice en base 1)
			cs.setInt(2, 100);
			
			// Registrar valor devolucion indicando el
			// tipo de dato SQL
			cs.registerOutParameter(1, Types.INTEGER);

			// Ejecutar subprograma
			cs.executeUpdate();

			// 6.- PROCESAR RESULTADOS EJECUCION
			// Recuperar valor del par�metro de salida
			int totalEjecutivos = cs.getInt(1);
			
			System.out.println("Total empleados departamento 100: " + 
					totalEjecutivos);

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();

		} finally {

			// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					System.out.println("No se puede cerrar conexion " + " con BBDD");
				}
			}
		}
	}

}

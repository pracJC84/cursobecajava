package curso.ejecucion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import curso.utilidades.JDBC;

/*
 * TRABAJO CON JDBC
 * 
 * 7 PASOS
 * =======
 * 
 * 1.- AGREGAR JAR CON LOS DRIVERS DEL FACBRICANTE DE BBDD
 * 
 * 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
 * 
 * 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
 * 
 * 4.- CREAR OBJETO DE EJECUCION DE SQL
 * 
 * 5.- EJECUTAR CODIGO SQL
 * 
 * 6.- PROCESAR RESULTADOS EJECUCION
 * 
 * 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
 */
public class Test {

	public static void main(String[] args) {
		
		try {
			// 2.- CARGAR CLASE CON LOS DRIVERS DE BBDD
			Class.forName(JDBC.DRIVERS);
			
		} catch (ClassNotFoundException e) {
			System.out.println("No se puede cargar la clase"
					+ " con los drivers de BBDD");
			
			System.exit(0);
		}

		// Conexion a BBDD
		Connection connection = null;
		
		
		try {
			// 3.- CREAR Y ESTABLECER CONEXION CON LA BBDD
			// Especificamos URL, USUARIO Y CLAVE
			connection = DriverManager.getConnection(
					JDBC.URL, JDBC.USER, JDBC.PASSWORD);
			
		} catch (SQLException e) {
			System.out.println("No se puede establecer conexion"
					+ " con BBDD");
			
			System.exit(0);
		}
		
		Statement statement = null;
				
		try {
			// 4.- CREAR OBJETO DE EJECUCION DE SQL
			// INSTRUCCIONES SQL => Statement
			statement = connection.createStatement();
			
		} catch (SQLException e) {
			System.out.println("No se puede crear objeto ejecucion"
					+ " SQL con BBDD");
			
			System.exit(0);
		}
		
		ResultSet rs = null;
		
		try {
			// 5.- EJECUTAR CODIGO SQL
			// EXECUTEQUERY => SELECT
			// EXECUTEUPDATE => NO SELECT
			rs = statement.executeQuery(JDBC.SELECT);
		} catch (SQLException e) {
			System.out.println("No se puede ejecutar codigo "
					+ " SQL con BBDD");
			
			System.exit(0);
		}
		
		// 6.- PROCESAR RESULTADOS EJECUCION
		// Desplazamiento al primer registro del ResultSet
		try {
			if(rs.next()) {
				// Al menos hay un registro
				int codigo = 0;
				String region = null;
				
				do {
					// Recuperar por nombre el valor de cada
					// columna de la fila apuntada por el cursor
					codigo = rs.getInt("REGION_ID");
					region = rs.getString("REGION_NAME");
					
					// Mostrar datos en pantalla
					System.out.println(codigo + "\t" + region);
					
				} while(rs.next()); // Volver a ejecutar mientras haya registros
			} else {
				System.out.println("No se han devuelto registros");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		// 7.- CERRAR CONEXION Y LIBERAR RECURSOS UTILIZADOS
		if(connection!=null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("No se puede cerrar conexion "
						+ " con BBDD");
			}
		}
	}

}
